import 'package:repository_pattern/models/news.dart';
import 'package:repository_pattern/services/base_response.dart';
import 'package:repository_pattern/services/news_service.dart';

class NewsRepositories {
  Future<List<News>> getNews() async {
    BaseResponse response = await NewsService().getNews();
    if (response.statusCode != 200) {
      return List();
    }

    var json = response.data;
    var listArticles = json['articles'] as List<dynamic>;
    List<News> listNews = listArticles.map((e) => News.fromJson(e)).toList();
    return listNews;
  }
}
