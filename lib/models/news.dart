import 'package:json_annotation/json_annotation.dart';
part 'news.g.dart';

@JsonSerializable()
class News {
  final String title;
  final String description;
  @JsonKey(name: 'urlToImage')
  final String image;
  final String url;

  News(this.title, this.description, this.image, this.url);

  factory News.fromJson(Map<String, dynamic> json) => _$NewsFromJson(json);
  Map<String, dynamic> toJson() => _$NewsToJson(this);
}
