import 'package:flutter/material.dart';
import 'package:repository_pattern/models/news.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailPage extends StatefulWidget {
  static const routeName = 'detail';
  const DetailPage({Key key}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  bool _showLoading = false;
  @override
  Widget build(BuildContext context) {
    final News news = ModalRoute.of(context).settings.arguments as News;

    return Scaffold(
      appBar: AppBar(title: Text(news.title)),
      body: Container(
        child: Stack(
          children: [
            Container(
              child: WebView(
                initialUrl: news.url,
                onPageStarted: (url) {
                  setState(() {
                    _showLoading = true;
                  });
                },
                onPageFinished: (url) {
                  setState(() {
                    _showLoading = false;
                  });
                },
              ),
            ),
            _showLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SizedBox.shrink()
          ],
        ),
      ),
    );
  }
}
