import 'package:flutter/material.dart';
import 'package:repository_pattern/models/news.dart';
import 'package:repository_pattern/repositories/news_repository.dart';
import 'package:repository_pattern/views/detail.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/';
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<RefreshIndicatorState> _refreshState = GlobalKey();
  List<News> _listNews = List();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _refreshState.currentState.show();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      body: Container(
          child: RefreshIndicator(
        onRefresh: () => onRefresh(),
        key: _refreshState,
        child: ListView.builder(
          itemCount: _listNews.length,
          itemBuilder: (context, index) => ListTile(
            onTap: () {
              Navigator.of(context)
                  .pushNamed(DetailPage.routeName, arguments: _listNews[index]);
            },
            leading: _listNews[index].image != null
                ? Container(
                    width: 100,
                    height: 100,
                    child: Image.network(
                      _listNews[index].image,
                      fit: BoxFit.fill,
                    ),
                  )
                : Container(
                    width: 100,
                    height: 100,
                    child: Center(
                      child: Icon(Icons.warning),
                    ),
                  ),
            title: Text(_listNews[index].title,
                maxLines: 1, overflow: TextOverflow.ellipsis),
            subtitle: Text(_listNews[index].description,
                maxLines: 2, overflow: TextOverflow.ellipsis),
          ),
        ),
      )),
    );
  }

  Future<void> onRefresh() async {
    List<News> data = await NewsRepositories().getNews();

    setState(() {
      _listNews = data;
    });
  }
}
