class BaseResponse {
  static const SERVICE_ERROR = 999;
  final int statusCode;
  final dynamic data;
  final String message;

  BaseResponse(this.statusCode, this.data, this.message);
}
