import 'package:dio/dio.dart';
import 'package:repository_pattern/services/base_response.dart';

class NewsService {
  Future<BaseResponse> getNews() async {
    try {
      var response = await Dio().get(
          'https://newsapi.org/v2/everything?q=tesla&from=2021-05-15&sortBy=publishedAt&apiKey=e7ddc72a5ee842e7a498af65476999f9');
      return BaseResponse(
          response.statusCode, response.data, response.statusMessage);
    } on DioError catch (e) {
      return BaseResponse(e.response.statusCode, null, e.message);
    } catch (e) {
      return BaseResponse(BaseResponse.SERVICE_ERROR, null, e);
    }
  }
}
